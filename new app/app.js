const http=require('http');
const express=require('express');
const bodyparser=require('body-parser');
const app=express();
const adminData=require('./routes/admin');
const shopRoutes=require('./routes/shop');
const path=require('path');

app.use(bodyparser.urlencoded({extended:false}));
app.set('view engine','pug');
app.set('views','views');
app.use('/admin',adminData.routes);
app.use(shopRoutes);
app.use(express.static(path.join(__dirname,'public')))

app.use((req,res,next)=>{
//	res.status(404).send("<h1>404 not found</h1>")
//	res.status(404).sendFile(path.join(__dirname,'views','404.html'));
	  res.status(404).render('404')

})

//const server=http.createServer(app);
app.listen(3000);